[PrefixDeclaration]
:   http://www.side-sante.fr/sides#
sp:   http://spinrdf.org/sp#
owl:    http://www.w3.org/2002/07/owl#
rdf:    http://www.w3.org/1999/02/22-rdf-syntax-ns#
spl:    http://spinrdf.org/spl#
xml:    http://www.w3.org/XML/1998/namespace
xsd:    http://www.w3.org/2001/XMLSchema#
rdfs:   http://www.w3.org/2000/01/rdf-schema#
spin:   http://spinrdf.org/spin#
sides:    http://www.side-sante.fr/sides#
wiki_sides: http://wiki.side-sante.fr/doku.php?id=sides:ref:

[SourceDeclaration]
sourceUri datasource1
connectionUrl jdbc:postgresql://88.99.95.22:9009/sides
username  postgres
password  MnjpWhMxxx9mv2P
driverClass org.postgresql.Driver

[MappingDeclaration] @collection [[

mappingId urn:test
target    sides:test{id} a sides:test .
source    select a.id as id, a.title, a.startdate, a.enddate from public.assessment a

mappingId urn:test_has_for_title
target    sides:test{id} sides:has_for_title "{title}"^^xsd:string .
source    select a.id as id, a.title as title, a.startdate, a.enddate from public.assessment a

mappingId urn:test_start_date
target    sides:test{id} sides:starting_date_of_test "{startdate}"^^xsd:dateTime .
source    select a.id as id, a.title, a.startdate as startdate, a.enddate as enddate from public.assessment a where a.startdate is not null 

mappingId urn:test_end_date
target    sides:test{id} sides:ending_date_of_test "{enddate}"^^xsd:dateTime .
source    select a.id as id, a.title, a.startdate as startdate, a.enddate as enddate from public.assessment a where a.enddate is not null


mappingId urn:relation_evaluation_type_question
target    sides:eval{pool_id} sides:has_for_question sides:q{question_id} .
source    select pq.pool_id as pool_id, pq.question_id as question_id, pq.position from pool_question pq
         inner join ontosides.evaluation ev on pq.pool_id = ev.pool_id

mappingId urn:relation_test_evaluation_type
target    sides:test{assessment_id} sides:is_made_of sides:eval{docimocontent_id} .
source    select dca.assessment_id as assessment_id, dca.docimocontent_id as docimocontent_id from docimocontentassessment dca
          inner join ontosides.evaluation ev on ev.pool_id = dca.docimocontent_id

mappingId urn:relation_test_student
target    sides:test{assessment_id} sides:has_for_registrant sides:stu{participant_id} .
source    select p.assessment_id as assessment_id, p.participant_id as participant_id from public.participant p

mappingId urn:student
target    sides:stu{participant_id} a sides:student ; sides:has_for_id "{participant_id}"^^xsd:integer .
source    select participant_id from ontosides.student

mappingId urn:proposal_of_answer
target    sides:prop{id} a sides:proposal_of_answer .
source    select c.id as id, c.question_id as question_id from public.choice c
      inner join ontosides.question q on c.question_id = q.question_id

mappingId urn:proposal_of_answer_valid
target    sides:prop{id} sides:has_for_correction "{valid}"^^xsd:boolean .
source    select c.id as id, ccc.valid, CASE ccc.valid WHEN 't' THEN 'true' WHEN 'f' THEN 'false' END as valid
          from public.choice c
          inner join correction.choice_correction ccc on c.correction_id = ccc.id
          inner join ontosides.question q on  c.question_id = q.question_id

mappingId urn:speciality
target    sides:{uri} a sides:speciality ; rdfs:label "{name}"@fr .
source    select s.uri as uri, me.name from meta_speciality me inner join ontosides.speciality s on s.speciality_id = me.id

mappingId urn:relation_eval_medical_specialty
target    sides:eval{docimocontent_id} sides:is_linked_to_the_medical_speciality sides:{uri} .
source    select ds.docimocontent_id as docimocontent_id, s.uri as uri from docimocontent_speciality ds
      inner join meta_speciality me on ds.speciality_id = me.id
      inner join ontosides.speciality s on s.speciality_id = me.id
      inner join docimocontent d on d.id = ds.docimocontent_id
      inner join ontosides.evaluation ev on d.id = ev.pool_id

mappingId urn:relation_question_medical_specialty
target    sides:q{docimocontent_id} sides:is_linked_to_the_medical_speciality sides:{uri} .
source    select ds.docimocontent_id as docimocontent_id, s.uri as uri from docimocontent_speciality ds
      inner join meta_speciality me on ds.speciality_id = me.id
      inner join ontosides.speciality s on s.speciality_id = me.id
      inner join docimocontent d on d.id = ds.docimocontent_id
      inner join ontosides.question q on d.id = q.question_id

mappingId urn:meta_cross_knowledge
target    sides:{uri} a sides:referential_entity ; rdfs:label "{name}"@fr .
source    select cke.cke_id as id, cke.uri as uri, mck.name as name from ontosides.cross_knowledge_entity cke inner join
          meta_cross_knowledge mck on mck.id = cke.cke_id

mappingId urn:relation_eval_cross_knowledge_entity
target    sides:eval{docimocontent_id} sides:is_linked_to_the_cross_knowledge_entity sides:{uri} .
source    select dc.docimocontent_id as docimocontent_id, cke.uri as uri from docimocontent_crossknowledge dc
      inner join ontosides.cross_knowledge_entity cke on cke.cke_id = dc.crossknowledge_id
      inner join docimocontent d on d.id = dc.docimocontent_id
      inner join ontosides.evaluation ev on d.id = ev.pool_id

mappingId urn:relation_question_cross_knowledge_entity
target    sides:q{docimocontent_id} sides:is_linked_to_the_cross_knowledge_entity sides:{uri} .
source          select dc.docimocontent_id as docimocontent_id, cke.uri as uri from docimocontent_crossknowledge dc
      inner join ontosides.cross_knowledge_entity cke on cke.cke_id = dc.crossknowledge_id
      inner join docimocontent d on d.id = dc.docimocontent_id
      inner join ontosides.question q on d.id = q.question_id

mappingId urn:question_QMA
target    sides:q{id} a sides:QMA ; sides:has_for_title "{title}"^^xsd:string .
source    select d.id as id, d.title as title, d.discr from public.docimocontent d
      inner join ontosides.question q on d.id = q.question_id
      where d.discr = 'qrm'

mappingId urn:question_QUA
target    sides:q{id} a sides:QUA ; sides:has_for_title "{title}"^^xsd:string .
source    select d.id as id, d.title as title, d.discr from public.docimocontent d
      inner join ontosides.question q on d.id = q.question_id
      where d.discr = 'qru'

mappingId urn:question_QSOA
target    sides:q{id} a sides:QSOA ; sides:has_for_title "{title}"^^xsd:string .
source    select d.id as id, d.title as title, d.discr from public.docimocontent d
      inner join ontosides.question q on d.id = q.question_id
      where d.discr = 'textq'

mappingId urn:question_TCS_question
target    sides:q{id} a sides:TCS_question ; sides:has_for_title "{title}"^^xsd:string .
source    select d.id as id, d.title as title, d.discr from public.docimocontent d
         inner join ontosides.question q on d.id = q.question_id
         where d.discr = 'tcsquestion'


mappingId urn:evaluation_type_set_isolation_question
target    sides:eval{id} a sides:set_of_isolated_questions ; sides:has_for_title "{title}"^^xsd:string .
source    select d.id as id, d.title as title, d.discr from public.docimocontent d
      inner join ontosides.evaluation ev on d.id = ev.pool_id where d.discr = 'qi'

mappingId urn:evaluation_type_progressive_clinical_case
target    sides:eval{id} a sides:progressive_clinical_case ; sides:has_for_title "{title}"^^xsd:string .
source    select d.id as id, d.title as title, d.discr from public.docimocontent d
      inner join ontosides.evaluation ev on d.id = ev.pool_id where d.discr = 'dp'

mappingId urn:evaluation_type_TCS
target    sides:eval{id} a sides:TCS ; sides:has_for_title "{title}"^^xsd:string .
source    select d.id as id, d.title as title, d.discr from public.docimocontent d
      inner join ontosides.evaluation ev on d.id = ev.pool_id where d.discr = 'tcspool'

mappingId urn:evaluation_type_LCA
target    sides:eval{id} a sides:LCA ; sides:has_for_title "{title}"^^xsd:string .
source    select d.id as id, d.title as title, d.discr from public.docimocontent d
      inner join ontosides.evaluation ev on d.id = ev.pool_id where d.discr = 'lca'


mappingId urn:relation_question_learning_objective
target    sides:q{resourceid} sides:is_linked_to_ECN_referential_entity sides:learning_objective_{code} .
source    select st.resourceid as resourceid, lo.code as code from public.skilllink st
      inner join ontosides.learning_objective lo on  lo.id = st.skillid
      inner join ontosides.question q on st.resourceid = q.question_id

mappingId urn:relation_question_learning_sub_objective
target    sides:q{resourceid} sides:is_linked_to_ECN_referential_entity sides:learning_sub_objective_{code_parent}_{code_child} .
source    select st.resourceid as resourceid, lso.code_parent as code_parent,  lso.code_child as code_child from public.skilllink st
      inner join ontosides.learning_sub_objective lso on  lso.id_child = st.skillid
      inner join ontosides.question q on st.resourceid = q.question_id

mappingId urn:relation_eval_learning_sub_objective
target    sides:eval{resourceid} sides:is_linked_to_ECN_referential_entity sides:learning_sub_objective_{code_parent}_{code_child} .
source    select st.resourceid as resourceid, lso.code_parent as code_parent,  lso.code_child as code_child from public.skilllink st
      inner join ontosides.learning_sub_objective lso on  lso.id_child = st.skillid
      inner join ontosides.evaluation ev on st.resourceid = ev.pool_id

mappingId urn:relation_eval_learning_objective
target    sides:eval{resourceid} sides:is_linked_to_ECN_referential_entity sides:learning_objective_{code} .
source    select st.resourceid as resourceid, lo.code as code from public.skilllink st
      inner join ontosides.learning_objective lo on  lo.id = st.skillid
      inner join ontosides.evaluation ev on st.resourceid = ev.pool_id

mappingId urn:sequence_of_questions_by_evaluation
target    sides:lq{pool_id} rdf:_{pos} sides:q{question_id} .
source    select pq.pool_id as pool_id, pq.question_id as question_id, (pq.position +1) as pos from public.pool_question pq
      inner join ontosides.evaluation ev on ev.pool_id = pq.pool_id

mappingId urn:relation_evaluation_list_of_questions
target    sides:lq{pool_id} a rdf:Seq . sides:eval{pool_id} sides:has_for_list_of_questions sides:lq{pool_id} .
source    select ev.pool_id as pool_id from ontosides.evaluation ev inner join
      public.docimocontent d on d.id = ev.pool_id where d.discr = 'dp'


mappingId urn:action_to_answer
target    sides:adr{response_id_new} a sides:action_to_answer .
source    select response_id_new, begindate from ontosides.response

mappingId urn:relation_action_to_answer_test
target    sides:adr{response_id_new} sides:done_during sides:test{assessment_id} .
source    select response_id_new, assessment_id from ontosides.response

mappingId urn:relation_answer_question
target    sides:answer{response_id} sides:correspond_to_question sides:q{question_id} .
source    select concat(cast(response_id as text), cast(choice_id as text)) as response_id_new,
          rt.id as response_id, rc.choice_id as choice_id, c.question_id as question_id
          from response.responses_choices rc
          inner join response.response_table rt on rt.id = rc.response_id
          inner join choice c on c.id = rc.choice_id
          inner join correction.choice_correction ccc on ccc.id = c.correction_id


mappingId urn:action_to_answer
target    sides:adr{response_id_new} a sides:action_to_answer ; sides:has_for_timestamp "{begindate}"^^xsd:dateTime .
source    select response_id_new, begindate from ontosides.response

mappingId urn:relation_action_to_answer_test
target    sides:adr{response_id_new} sides:done_during sides:test{assessment_id} .
source    select response_id_new, assessment_id from ontosides.response

mappingId urn:relation_action_to_answer_question
target    sides:adr{response_id_new} sides:correspond_to_question sides:q{question_id} .
source    select concat(cast(response_id as text), cast(choice_id as text)) as response_id_new, 
          rt.id as response_id, rc.choice_id as choice_id, c.question_id as question_id
          from response.responses_choices rc
          inner join response.response_table rt on rt.id = rc.response_id
          inner join choice c on c.id = rc.choice_id
          inner join correction.choice_correction ccc on ccc.id = c.correction_id

mappingId urn:relation_action_to_answer_student
target    sides:adr{response_id_new} sides:done_by sides:stu{participant_id} .
source    select response_id_new, participant_id from ontosides.response

mappingId urn:relation_has_rightly_ticked
target    sides:adr{response_id_new} sides:has_rightly_ticked sides:prop{choice_id} .
source    select response_id_new, choice_id ,valid from ontosides.response r where r.valid = true

mappingId urn:relation_has_wrongly_ticked
target    sides:adr{response_id_new} sides:has_wrongly_ticked sides:prop{choice_id} .
source    select response_id_new, choice_id, valid from ontosides.response r where r.invalid = true

mappingId urn:relation_has_missed_right_tick
target    sides:adr{response_id_new} sides:has_missed_right_tick sides:prop{choice_id} .
source    select concat(cast(response_id as text), cast(choice_id as text)) as response_id_new, choice_id from ontosides.missed_right_tick


mappingId urn:picture
target    sides:picture{file_id} a sides:picture .
source    select id, file_id from (
            select id, file_id from ontosides.docimocontent_comment_images
            union
            select id, file_id from ontosides.docimocontent_introduction_images
            union
            select id, file_id from ontosides.docimocontent_statement_images
            ) as t

mappingId urn:video
target    sides:video{file_id} a sides:video .
source    select id, file_id from (
            select id, file_id from ontosides.docimocontent_comment_videos
            union
            select id, file_id from ontosides.docimocontent_introduction_videos
            union
            select id, file_id from ontosides.docimocontent_statement_videos
            ) as t


]]